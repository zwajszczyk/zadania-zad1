# -*- encoding: utf-8 -*-
import socket
import sys

# ustalenie gniazda nasluchujacego serwera
server_address = ('194.29.175.240', 31011) #wersja zdalna
#server_address = ('localhost', 31011) #wersja do testow na localhoscie

while True: #program dziala w petli

    try:
        try:
            number1 = float(raw_input(u'Podaj liczbe 1:'))
        except ValueError:
            print u'To nie jest liczba!'
            continue # jesli wprowadzone dane nie byly liczba, zaczynamy petle od poczatku
        try:
            number2 = float(raw_input(u'Podaj liczbe 2:'))
        except ValueError:
            print u'To nie jest liczba!'
            continue
        message=str(number1)+'_'+str(number2) #przesylamy liczby jako stringi
        #teraz tworzenie socketa,bo:
        #-wczesniej nie byl potrzebny
        #-zdaje sie ze nie da sie zamknac socketa, a potem ponownie do niego podlaczyc, dlatego
        # przy kazdym polaczeniu trzeba tworzyc socketa na nowo
        clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_IP)
        clientSocket.connect(server_address)
        clientSocket.sendall(message.encode('utf-8'))
        resp = clientSocket.recv(200) #otrzymujemy wynik z serwera
        print u'suma naszych liczb to:' + resp
        clientSocket.close() #polaczenie skonczone, zamykamy socketa

    finally:

        ifContinue=str(raw_input(u'Czy chcesz kontynuowac? (y for yes/any key for no)'))
        if ifContinue=='y': #jesli chcemy kontynuowac, to petla sie zapetla
            pass
        else:
            break #a jesli nie, to klient konczy dzialanie
