# -*- encoding: utf-8 -*-
from _ast import While
from _socket import SOCK_STREAM

import socket
import re
#tworzenie gniazda i powiazanie go z adresem
serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_IP)
#server_address = ('localhost', 31011) # wersja do testow na localhoscie
server_address = ('194.29.175.240', 31011) #wersja zdalna
serverSocket.bind(server_address)
#nasluchiwanie przychodzacych polaczen
serverSocket.listen(1)

while True:
    #czekanie na polaczenia
    connection, clientAddress = serverSocket.accept()

    try:
        recvedData = connection.recv(200)
        recvedDataList = recvedData.split('_') #podzielenie danych od klienta na dwie liczby w formie stringow
        mySum = float(recvedDataList[0]) + float(recvedDataList[1]) #dodanie tych liczb
        connection.sendall(str(mySum)) #wyslanie do klienta
        pass

    finally:
        connection.close() #zamkniecie polaczenia
