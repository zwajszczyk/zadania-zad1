# -*- encoding: utf-8 -*-

import socket

# Ustawienie licznika na zero

licznik = 0

# Tworzenie gniazda TCP/IP

socket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_IP)

# Powiązanie gniazda z adresem
#server_address = ('194.29.175.240', 31011)  # TODO: zmienić port!
server_address = ('localhost', 31011)
socket1.bind(server_address)

# Nasłuchiwanie przychodzących połączeń
socket1.listen(1)

while True:
    # Czekanie na połączenie
    connection, client_address =socket1.accept()

    # Podbicie licznika
    licznik +=1

    try:
        # Wysłanie wartości licznika do klienta
        connection.sendall(str(licznik))
        pass

    finally:
        # Zamknięcie połączenia
        connection.close()
        pass
