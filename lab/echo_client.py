# -*- encoding: utf-8 -*-

import socket

# Tworzenie gniazda TCP/IP

socket2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_IP)

# Połączenie z gniazdem nasłuchującego serwera
#server_address = ('194.29.175.240', 31011)  # TODO: zmienić port!
server_address = ('localhost', 31011)
socket2.connect(server_address)

try:
    # Wysłanie danych
    message = u'To jest wiadomość, która zostanie zwrócona.'
    socket2.sendall(message.encode('utf-8'))

    # Wypisanie odpowiedzi
    resp = socket2.recv(1000)
    print resp

finally:
    # Zamknięcie połączenia
    socket2.close()
    pass